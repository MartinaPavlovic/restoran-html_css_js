var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('hrana', {
        linkActive: 'hrana',
        title: 'Hrana'
    });
});

module.exports = router;