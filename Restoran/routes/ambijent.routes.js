var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('ambijent', {
        linkActive: 'ambijent',
        title: 'Ambijent'
    });
});

module.exports = router;