var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('vjencanja', {
        linkActive: 'vjencanja',
        title: 'Vjencanja'
    });
});

module.exports = router;