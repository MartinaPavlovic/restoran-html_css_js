var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.render('restoran', {
        linkActive: 'restoran',
        title: 'Restoran'
    });
});

module.exports = router;