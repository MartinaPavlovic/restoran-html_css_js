const express = require('express');
const app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')));


const homeRouter = require('./routes/home.routes');
const ambijentRouter = require('./routes/ambijent.routes');
const hranaRouter = require('./routes/hrana.routes');
const OnamaRouter = require('./routes/Onama.routes');
const restoranRouter = require('./routes/restoran.routes');
const vjencanjaRouter = require('./routes/vjencanja.routes');


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use('/', homeRouter);
app.use('/ambijent', ambijentRouter);
app.use('/hrana', hranaRouter);
app.use('/Onama', OnamaRouter);
app.use('/restoran', restoranRouter);
app.use('/vjencanja', vjencanjaRouter);




app.listen(5000);